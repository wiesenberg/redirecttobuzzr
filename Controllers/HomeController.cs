﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RedirectToBuzzr.Models;

namespace RedirectToBuzzr.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ParseShorUrlID(string shorUrlSuffix)
        {

            if (Request.IsInValidIp(5))
            {
                var r = ConfigurationManager.AppSettings["buzzrUrl"];
                return Redirect(r);
            }

            string redirectUrl = ConfigurationManager.AppSettings["buzzrTestSite"];
            string redirectTo = "";

            using (var model = new ShortUrlEntities())
            {
                redirectTo = (from m
                              in model.ShortToLongUrls
                              where m.shortUrlSuffix.ToLower() == shorUrlSuffix.ToLower()
                              select m.longUrl).FirstOrDefault();
            }

            if (!string.IsNullOrEmpty(redirectTo))
            {
                return Redirect(redirectTo);  //@TempData["url"] = redirectTo;
            }
            else
            {
                // Failed request. add the ip to the failed list and redirect to buzzr website.
                Request.AddIp();
                redirectUrl = ConfigurationManager.AppSettings["buzzrUrl"];
                return Redirect(redirectUrl);
            }

                

                //the redirect moved to client side
               // return Redirect(redirectTo);       

            //TempData["urlNotFoundMessage"] = "הכתובת שהוזנה אינה תקנית";
            //return View();
        }
    }
}
