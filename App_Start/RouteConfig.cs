﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RedirectToBuzzr
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "Default",
               url: "",
               defaults: new { controller = "Home", action = "Index" }
           );

            routes.MapRoute(
                name: "shorUrlSuffix",
                url: "{shorUrlSuffix}",
                defaults: new { controller = "Home", action = "ParseShorUrlID", id = "0" }
            );
        }
    }
}