﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RedirectToBuzzr.Models
{
    /// <summary>
    /// This class is used to count ip addresses in a list. We want to count ip address which created a failed request so we can later on block them from creating a
    /// normal request. we will make them use the reCAPTCHA or block in some other way. "failed request" is a request which didn't find any result.
    /// </summary>
    public static class FailedIpManager
    {
        private static List<IpRequest> _ipManager;
        private static DateTime _lastReset = DateTime.Now;
        private const int MAX_FAILED_RECORDS_PER_IP = 50;
        private const int HOURS_TO_RESET_LIST = 2;
        private const int MAX_RECORDS_TO_KEEP_IN_LIST = 1000;
        private const int NUMBERS_OF_RECORDS_TO_KEEP = 500;
        private const int MINUTES_AGE_OF_RECORD_TO_KEEP = -45; // MUST be negative number

        static FailedIpManager()
        {
            _ipManager = new List<IpRequest>();
        }

        /// <summary>
        /// Add the ip address (if exists) of the current HTTP request to the failed request list.
        /// </summary>
        /// <param name="request">The Current HTTP Request</param>
        public static void AddIp(this HttpRequestBase request)
        {
            ResetIpList();

            var ip = request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? request.ServerVariables["HTTP_CLIENT_IP"] ?? request.ServerVariables["REMOTE_ADDR"] ?? null;

            if (!string.IsNullOrEmpty(ip))
            {
                // If we have less than 50 (MAX_FAILED_RECORDS_PER_IP) requests from this ip - add the ip to the list
                if (_ipManager.Count(i => i.Ip == ip) < MAX_FAILED_RECORDS_PER_IP)
                    _ipManager.Add(new IpRequest(ip));
                else
                {   // else, we choose the last entry, remove it and add a new entry instead.
                    var last = _ipManager.Last(l => l.Ip == ip);
                    if (last != null)
                    {
                        _ipManager.Remove(last);
                        _ipManager.Add(new IpRequest(ip));
                    }
                }
            }

            // if we have more than 1000 entries -> remove all the entries until 45 minutes ago.
            if (_ipManager.Count > MAX_RECORDS_TO_KEEP_IN_LIST)
            {
                _ipManager.RemoveAll(a => a.Date < DateTime.Now.AddMinutes(MINUTES_AGE_OF_RECORD_TO_KEEP));

                // if still we have more than 1000 entries -> remove everything but the top 500.
                var top500 = _ipManager.OrderBy(a => a.Date).Take(NUMBERS_OF_RECORDS_TO_KEEP).ToList();
                _ipManager.Clear();
                _ipManager = top500;
            }
        }

        /// <summary>
        /// Check if this ip is invalid. If the request came from an Ip which is in the list 3 times or more (or numberOfFailedRequests value)
        /// then this ip is invalid and we will return true. else - return false
        /// </summary>
        /// <param name="request">The current HTTP Request</param>
        /// <param name="numberOfFailedRequests">Number of occurences in the list to indicate this ip address is invalid.</param>
        /// <returns></returns>
        public static bool IsInValidIp(this HttpRequestBase request, int numberOfFailedRequests = 3, int minutesToBlock = 30)
        {
            ResetIpList();

            // To prevent sending a number greater than the "cleaning" more than X records in the AddIp method.
            if (numberOfFailedRequests > MAX_FAILED_RECORDS_PER_IP)
                numberOfFailedRequests = MAX_FAILED_RECORDS_PER_IP - 5;

            var ip = request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? request.ServerVariables["HTTP_CLIENT_IP"] ?? request.ServerVariables["REMOTE_ADDR"] ?? null;

            if (_ipManager.Count(a => a.Ip == ip) >= numberOfFailedRequests)
            {

                DateTime lastIpEntry = DateTime.MinValue;
                var ipOccurences = _ipManager.Where(a => a.Ip == ip);
                if (ipOccurences.Count() > 0)
                {
                    lastIpEntry = ipOccurences.Max(t => t.Date);
                    TimeSpan span = DateTime.Now - lastIpEntry;
                    if (span.Minutes < minutesToBlock)
                        return true; // Invalid
                    else
                    {
                        return false; // Valid
                    }
                }
                else // if for some reason we didn't find. we have some kind of an error. return invalid.
                    return true;
            }
            else
                return false;
        }



        /// <summary>
        /// Add the ip to the list and return the count of this ip in the list
        /// </summary>
        /// <param name="request">The Request</param>
        /// <returns>The count of the current ip in the list</returns>
        public static int AddAndCountIP(this HttpRequestBase request)
        {
            ResetIpList();
            AddIp(request);

            return CountIP(request);
        }

        /// <summary>
        /// Count the number of times an IP address occur in the list of failed request ip.
        /// </summary>
        /// <param name="request">Current HTTP Request</param>
        /// <returns>Number of occurences of the ip address in the list</returns>
        public static int CountIP(this HttpRequestBase request)
        {
            ResetIpList();

            var ip = request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? request.ServerVariables["HTTP_CLIENT_IP"] ?? request.ServerVariables["REMOTE_ADDR"] ?? null;

            return _ipManager.Count(a => a.Ip == ip);
        }


        /// <summary>
        /// return the client ip from the variables: HTTP_X_FORWARDED_FOR, or HTTP_CLIENT_IP or REMOTE_ADDR. If didn't find anything - returns null.
        /// </summary>
        /// <param name="request">Current HTTP Request</param>
        /// <returns>ip address or null if didn't find.</returns>
        public static string ClientIp(this HttpRequestBase request)
        {
            return request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? request.ServerVariables["HTTP_CLIENT_IP"] ?? request.ServerVariables["REMOTE_ADDR"] ?? null;
        }

        // If we past 6 hours we reset the list to remove all Ip address and allow access again to all.
        private static void ResetIpList()
        {
            TimeSpan span = DateTime.Now - _lastReset;

            if (span.Hours > HOURS_TO_RESET_LIST)
            {
                _lastReset = DateTime.Now;
                _ipManager.Clear();
            }
        }

    }

    public class IpRequest
    {
        public string Ip { get; set; }
        public DateTime Date { get; set; }

        public IpRequest()
        {
            Date = DateTime.Now;
        }

        public IpRequest(string ip)
        {
            Date = DateTime.Now;
            Ip = ip;
        }
    }
}
